# Swagger: !(http://swagger.io)

## Configuration

(1) Maven dependency

		<dependency>
			<groupId>com.wordnik</groupId>
			<artifactId>swagger-jaxrs_2.10</artifactId>
			<version>1.3.6</version>
		</dependency>

(2) web.xml

	<context-param>
		<param-name>resteasy.providers</param-name>
		<param-value>
			com.wordnik.swagger.jaxrs.json.JacksonJsonProvider,
			com.wordnik.swagger.jaxrs.listing.ApiDeclarationProvider,
			com.wordnik.swagger.jaxrs.listing.ResourceListingProvider
		</param-value>
	</context-param>

	<context-param>
		<param-name>resteasy.resources</param-name>
		<param-value>
			com.wordnik.swagger.jaxrs.listing.ApiListingResourceJSON
		</param-value>
	</context-param>

	<servlet>
		<servlet-name>SwaggerBootstrap</servlet-name>
		<servlet-class>com.skein.ticket.servlet.SwaggerBootstrap
		</servlet-class>
		<load-on-startup>2</load-on-startup>
	</servlet>

(3) SwaggerBootstrap Servlet

(4) Annotations at resources classes like @Api, @ApiOperation, @ApiParam, @ApiModel

(5) Download swagger-ui from https://github.com/wordnik/swagger-ui. Extract and copy contents from diet folder into web app

(6) Customize url according to environment in swagger-index.html

(7) Access ui @ http://localhost:9966/ticket-resource/swagger-index.html