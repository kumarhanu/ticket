package com.skein.ticket.persistent;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsInstanceOf.instanceOf;

import java.sql.SQLException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.skein.ticket.persistent.config.HSQLDBPersistenceConfig;


/**
 * Unit test for PersistenceConfig.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={HSQLDBPersistenceConfig.class})
@ActiveProfiles(profiles = "dev")
public class HSQLDBPersistenceConfigTest
{
	@Autowired
	HibernateTransactionManager manager;
	
	@Test
	public void testTransactionManager() throws SQLException{
		assertThat(manager, instanceOf(HibernateTransactionManager.class));
	}
  
}
