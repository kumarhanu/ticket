package com.skein.ticket.persistent.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.FetchProfile;
import org.hibernate.annotations.FetchProfiles;

@FetchProfiles({ @FetchProfile(name = UserEntity.PROFILE_USER_WITH_TICKETS, fetchOverrides = { @FetchProfile.FetchOverride(entity = UserEntity.class, association = "tickets", mode = FetchMode.JOIN) }) })
@NamedQueries({ @NamedQuery(name = UserEntity.QUERY_ID_DELETE_USER, query = UserEntity.QUERY_DELETE_USER) })
@Entity
@Table(name = "USER", uniqueConstraints = { @UniqueConstraint(columnNames = "USER_NAME") })
public class UserEntity {

	static final String QUERY_ID_DELETE_USER = "deleveUser";
	public static final String QUERY_DELETE_USER = "delete UserEntity where user_id= :userId";

	public static final String PROFILE_USER_WITH_TICKETS = "user-with-tickets";

	private int userId;
	private String userName;
	private Set<TicketEntity> tickets;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "USER_ID", unique = true, nullable = false)
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	@Column(name = "USER_NAME")
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	/*
	 * One-to-Many Bi-direction association. A user can have multiple tickets.
	 * 
	 * mappedBy is the property to hold the user info in TicketEntity class.
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	public Set<TicketEntity> getTickets() {
		return tickets;
	}

	public void setTickets(Set<TicketEntity> tickets) {
		this.tickets = tickets;
	}
}