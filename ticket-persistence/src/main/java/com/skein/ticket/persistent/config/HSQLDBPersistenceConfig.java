package com.skein.ticket.persistent.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Profile("dev")
@Configuration
@EnableTransactionManagement
@ComponentScan({ "com.skein.ticket.persistent.dao.*",
		"com.skein.ticket.persistent.entity.*" })
@PropertySource({"classpath:hsqldb.properties", "classpath:hibernate.properties"})
public class HSQLDBPersistenceConfig {

	@Autowired
	Environment env;

	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource ds = new DriverManagerDataSource();
		ds.setDriverClassName(env.getProperty("driverclassname"));
		ds.setUrl(env.getProperty("url"));
		ds.setUsername(env.getProperty("username"));
		ds.setPassword(env.getProperty("password"));

		return ds;
	}

	@Bean
	public LocalSessionFactoryBean sessionFactory() {
		LocalSessionFactoryBean factory = new LocalSessionFactoryBean();
		factory.setPackagesToScan("com.skein.ticket.persistent.entity");
		factory.setDataSource(dataSource());
		factory.setHibernateProperties(hibernateProperties());

		return factory;
	}
	
	@Bean
	public HibernateTransactionManager transactionManager(){
		HibernateTransactionManager manager = new HibernateTransactionManager();
		manager.setSessionFactory(sessionFactory().getObject());
		return manager;
	}

	private Properties hibernateProperties() {
		return new Properties() {
			{
				setProperty("hibernate.archive.autodetection",
						env.getProperty("autodetection"));
				
				setProperty("hibernate.dialect",
						env.getProperty("dialect"));
				
				setProperty("hibernate.show_sql",
						env.getProperty("show_sql"));
				
				setProperty("hibernate.c3p0.min_size",
						env.getProperty("min_size"));
				
				setProperty("hibernate.c3p0.timeout",
						env.getProperty("timeout"));
				
				setProperty("hibernate.hbm2ddl.auto",
						env.getProperty("hbm2ddl.auto"));
			}
		};
	}
}
