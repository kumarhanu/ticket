package com.skein.ticket.persistent.dao;

import java.util.List;

import com.skein.ticket.persistent.entity.EventEntity;

public interface EventDao<T extends EventEntity> {

	public List<T> getAllEvents();
	
	public String create(T event);

	public T findById(int eventId);
	
	public T findByTicket(String ticketId);

	public void update(T event);

	public int delete(String eventId);
}
