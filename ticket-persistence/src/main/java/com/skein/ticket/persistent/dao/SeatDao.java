package com.skein.ticket.persistent.dao;

import java.util.List;

import com.skein.ticket.persistent.entity.SeatEntity;

public interface SeatDao<T extends SeatEntity> {

	public List<T> getAllSeats(List<String> fields, List<String> statusFilter,
			List<String> sortFields, int offSet, int limit);

	public String create(T seat);

	public T findById(int seatId);

	// public T findByEvent(int eventId);

	public void update(T seat);

	public int delete(String seatId);
	
	public long getCount(List<String> statusFilter);
}
