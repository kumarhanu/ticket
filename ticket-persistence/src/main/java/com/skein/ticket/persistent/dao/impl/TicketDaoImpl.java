package com.skein.ticket.persistent.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.skein.ticket.persistent.dao.EventDao;
import com.skein.ticket.persistent.dao.TicketDao;
import com.skein.ticket.persistent.entity.EventEntity;
import com.skein.ticket.persistent.entity.TicketEntity;

@Repository("ticketDao")
public class TicketDaoImpl implements TicketDao<TicketEntity> {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<TicketEntity> getAllTickets() {
		Session session = sessionFactory.getCurrentSession();
		
		session.enableFetchProfile(TicketEntity.PROFILE_TICKET_WITH_EVENT);
		
		return session.createCriteria(TicketEntity.class).list();
	}

	@Override
	public String create(TicketEntity ticket) {
		return sessionFactory.getCurrentSession().save(ticket).toString();
	}

	@Override
	public TicketEntity findById(int ticketId) {
		return (TicketEntity) sessionFactory.getCurrentSession().get(
				TicketEntity.class, ticketId);
	}

	@Override
	public void update(TicketEntity ticket) {
		sessionFactory.getCurrentSession().update(ticket);
	}

	@Override
	public int delete(String ticketId) {
		int result = sessionFactory.getCurrentSession()
				.getNamedQuery(TicketEntity.QUERY_ID_DELETE_TICKET)
				.setString("ticketId", ticketId).executeUpdate();
		return result;
	}
}
