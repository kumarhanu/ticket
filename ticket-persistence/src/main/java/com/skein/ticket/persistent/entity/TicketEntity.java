package com.skein.ticket.persistent.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.FetchProfile;
import org.hibernate.annotations.FetchProfiles;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@FetchProfiles({
		@FetchProfile(name = TicketEntity.PROFILE_TICKET_WITH_USER, fetchOverrides = { @FetchProfile.FetchOverride(entity = TicketEntity.class, association = "user", mode = FetchMode.JOIN) }),
		@FetchProfile(name = TicketEntity.PROFILE_TICKET_WITH_EVENT, fetchOverrides = { @FetchProfile.FetchOverride(entity = TicketEntity.class, association = "event", mode = FetchMode.JOIN) }),
		@FetchProfile(name = "aaaaa", fetchOverrides = { @FetchProfile.FetchOverride(entity = TicketEntity.class, association = "reservations", mode = FetchMode.JOIN) }), })

@NamedQueries({
		@NamedQuery(name = TicketEntity.QUERY_ID_DELETE_TICKET, query = TicketEntity.QUERY_DELETE_TICKET) })
@Entity
@Table(name = "TICKET")
public class TicketEntity {

	public static final String QUERY_ID_DELETE_TICKET = "deleteTicketById";
	static final String QUERY_DELETE_TICKET = "delete TicketEntity where ticket_id= :ticketId";

	public static final String PROFILE_TICKET_WITH_USER = "ticket-with-user";
	public static final String PROFILE_TICKET_WITH_EVENT = "ticket-with-event";

	private int ticketId;
	private EventEntity event;
	private Set<ReservationEntity> reservations;

	/*
	 * CascadeType.SAVE_UPDATE will create Reservations also when a Ticket gets
	 * created.
	 * 
	 * Use hibernate CascadeType.SAVE_UPDATE instead of javax.persistence CascadeType as it
	 * doesn't have SAVE option.
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "ticket")
	@Cascade({ org.hibernate.annotations.CascadeType.SAVE_UPDATE})
	@OnDelete(action=OnDeleteAction.CASCADE)
	public Set<ReservationEntity> getReservations() {
		return reservations;
	}

	public void setReservations(Set<ReservationEntity> reservations) {
		this.reservations = reservations;
	}

	private UserEntity user;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "TICKET_ID")
	public int getTicketId() {
		return ticketId;
	}

	public void setTicketId(int ticketId) {
		this.ticketId = ticketId;
	}

	/*
	 * A Ticket is associated to one Event. An Event contain many Tickets.
	 * 
	 * So, Ticket table has a column to preserve Event Id. Where as Event table
	 * doesn't hold any column for Ticket association.
	 * 
	 * Bi-directional Many-to-One Association without Join table.
	 * 
	 * By default ManyToOne fetch type is EAGER.
	 * 
	 * Do not do Cascade.Save on this association. It will delete record from
	 * Event and insert.
	 */

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "EVENT_ID", nullable=true)
	// ID column of Event table
	@OnDelete(action=OnDeleteAction.CASCADE)
	public EventEntity getEvent() {
		return event;
	}

	public void setEvent(EventEntity event) {
		this.event = event;
	}

	/*
	 * Bi-directional Many-to-One association.
	 * 
	 * Column "USER" holds the user-id.
	 * 
	 * Do not do Cascade.Save on this association. It will delete record from
	 * Event and insert.
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_ID")
	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

}
