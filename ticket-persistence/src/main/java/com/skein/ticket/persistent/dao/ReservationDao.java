package com.skein.ticket.persistent.dao;

import java.util.List;
import java.util.Set;

import com.skein.ticket.persistent.entity.ReservationEntity;

public interface ReservationDao<T extends ReservationEntity> {

	public List<T> getAllReservations();

	public String create(T reservation);

	public T findById(int reservationId);

	public void update(T reservation);
}
