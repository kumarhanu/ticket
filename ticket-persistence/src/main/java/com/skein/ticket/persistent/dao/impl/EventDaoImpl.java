package com.skein.ticket.persistent.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.skein.ticket.persistent.dao.EventDao;
import com.skein.ticket.persistent.entity.EventEntity;
import com.skein.ticket.persistent.entity.SeatEntity;

@Repository("eventDao")
public class EventDaoImpl implements EventDao<EventEntity> {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<EventEntity> getAllEvents() {
		return sessionFactory.getCurrentSession()
				.createCriteria(EventEntity.class).list();
	}

	@Override
	public String create(EventEntity event) {
		return sessionFactory.getCurrentSession().save(event).toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.skein.ticket.persistent.dao.EventDao#findById(int)
	 */
	@Override
	public EventEntity findById(int eventId) {

		Property id = Property.forName("id");
		Session session = sessionFactory.getCurrentSession();

		Criteria criteria = session.createCriteria(EventEntity.class);
		criteria.add(id.eq(eventId));

		// Tickets are not loaded by default as FetchMode.LAZY has been set.
		// While loading a single Event, load the tickets as well. There are
		// three ways:
		// (1) Setting fetchMode to criteria
		// (2) By telling association in Hibernate.initialize
		// (3) Using Fetch Profiles

		// (1)
		// criteria.setFetchMode("tickets", FetchMode.JOIN);

		// (2)
		// Hibernate.initialize(event.getTickets());

		// (3) Fetch Profiles
		session.enableFetchProfile(EventEntity.PROFILE_EVENT_WITH_SEATS);

		EventEntity event = (EventEntity) criteria.uniqueResult();
		return event;
	}

	
	@Override
	public EventEntity findByTicket(String ticketId) {

		Session session = sessionFactory.getCurrentSession();

		session.enableFetchProfile(EventEntity.PROFILE_EVENT_WITH_SEATS);
		EventEntity event = (EventEntity) session.getNamedQuery(EventEntity.QUERY_ID_EVENT_BY_TICKET).setString("ticketId", ticketId).uniqueResult();
		
		return event;
	}

	
	@Override
	public void update(EventEntity event) {
		if(event.getSeats() != null){
			for(SeatEntity seat:event.getSeats()){
				seat.setEvent(event);
			}
		}
		sessionFactory.getCurrentSession().update(event);
	}

	@Override
	public int delete(String eventId) {
		Session session = sessionFactory.getCurrentSession();
		int result = session.getNamedQuery(EventEntity.QUERY_ID_DELETE_EVENT)
				.setString("eventId", eventId).executeUpdate();
		return result;
	}

}
