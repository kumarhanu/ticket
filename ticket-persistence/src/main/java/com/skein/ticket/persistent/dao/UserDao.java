package com.skein.ticket.persistent.dao;

import java.util.List;

import com.skein.ticket.persistent.entity.UserEntity;

public interface UserDao<T extends UserEntity> {

	public List<T> getAllUsers();

	public String create(T user);

	public T findById(int userId);

	public void update(T user);

	public int delete(String userId);
}
