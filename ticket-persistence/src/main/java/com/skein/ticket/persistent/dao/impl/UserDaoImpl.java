package com.skein.ticket.persistent.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.skein.ticket.persistent.dao.EventDao;
import com.skein.ticket.persistent.dao.UserDao;
import com.skein.ticket.persistent.entity.EventEntity;
import com.skein.ticket.persistent.entity.UserEntity;

@Repository("userDao")
public class UserDaoImpl implements UserDao<UserEntity> {

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	EventDao<EventEntity> eventDao;

	@Override
	public List<UserEntity> getAllUsers() {
		return sessionFactory.getCurrentSession().createCriteria(
				UserEntity.class).list();
	}
	
	@Transactional
	@Override
	public String create(UserEntity user) {
		return sessionFactory.getCurrentSession().save(user).toString();
	}

	@Override
	public UserEntity findById(int userId) {
		Session session = sessionFactory.getCurrentSession();
		session.enableFetchProfile(UserEntity.PROFILE_USER_WITH_TICKETS);

		Property id = Property.forName("id");		
		Criteria criteria = session.createCriteria(UserEntity.class);
		criteria.add(id.eq(userId));
		
		UserEntity user = (UserEntity) criteria.uniqueResult();

		return user;
	}

	@Transactional
	@Override
	public void update(UserEntity user) {
		sessionFactory.getCurrentSession().update(user);
	}

	@Transactional
	@Override
	public int delete(String userId) {
		return sessionFactory.getCurrentSession().getNamedQuery(UserEntity.QUERY_DELETE_USER)
				.setString("userId", userId)
				.executeUpdate();
	}
}
