package com.skein.ticket.persistent.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.FilterDefs;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.SelectBeforeUpdate;

import com.skein.ticket.core.enums.SeatStatus;

@FilterDefs(@FilterDef(name = "status", parameters = @ParamDef(name = "status", type = "string")))
@Filters(@Filter(name = "status", condition = "SEAT_STATUS in (:status)"))
@NamedQueries({ @NamedQuery(name = SeatEntity.QUERY_ID_DELETE_SEAT, query = SeatEntity.QUERY_DELETE_SEAT) , @NamedQuery(name=SeatEntity.QUERY_ID_GET_SEAT_COUNT , query=SeatEntity.QUERY_GET_SEAT_COUNT)})
@DynamicUpdate
@SelectBeforeUpdate
@Entity
@Table(name = "SEAT")
public class SeatEntity {

	public static final String QUERY_ID_DELETE_SEAT = "deleteSeat";
	static final String QUERY_DELETE_SEAT = "delete SeatEntity where seatId = :seatId";
	
	public static final String QUERY_ID_GET_SEAT_COUNT = "getSeatCount";
	static final String QUERY_GET_SEAT_COUNT = "select count(seat.id) from SeatEntity seat";


	private int seatId;
	private String seatNumber;
	private SeatStatus status = SeatStatus.NOT_ALLOCATED;
	private EventEntity event;
	private ReservationEntity reservation;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "SEAT_ID", unique = true, nullable = false)
	public int getSeatId() {
		return seatId;
	}

	public void setSeatId(int seatId) {
		this.seatId = seatId;
	}

	@Column(name = "SEAT_NUMBER")
	public String getSeatNumber() {
		return seatNumber;
	}

	public void setSeatNumber(String seatNumber) {
		this.seatNumber = seatNumber;
	}

	@Column(name = "SEAT_STATUS")
	public SeatStatus getStatus() {
		return status;
	}

	public void setStatus(SeatStatus status) {
		this.status = status;
	}
	
	/*
	 *Seat is the owner in Seat-Event Association
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "EVENT_ID")
	public EventEntity getEvent() {
		return event;
	}

	public void setEvent(EventEntity event) {
		this.event = event;
	}

	@OneToOne
	public ReservationEntity getReservation() {
		return reservation;
	}

	public void setReservation(ReservationEntity reservation) {
		this.reservation = reservation;
	}

}
