package com.skein.ticket.persistent.dao;

import java.util.List;

import com.skein.ticket.persistent.entity.TicketEntity;

public interface TicketDao<T extends TicketEntity> {

	public List<T> getAllTickets();
	
	public String create(T ticket);

	public T findById(int ticketId);

	public void update(T ticket);

	public int delete(String ticketId);
}
