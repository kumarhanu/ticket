package com.skein.ticket.persistent.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.skein.ticket.persistent.dao.SeatDao;
import com.skein.ticket.persistent.entity.SeatEntity;

@Repository("seatDao")
public class SeatDaoImpl implements SeatDao<SeatEntity> {

	@Autowired
	private SessionFactory sessionFactory;
	private static final char SORT_BY_ORDER = '-';

	@Override
	public List<SeatEntity> getAllSeats(List<String> fields,
			List<String> statusFilter, List<String> sortFields, int offSet,
			int limit) {

		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(SeatEntity.class);

		// Setting select fields
		if (fields != null) {
			ProjectionList projectionList = Projections.projectionList();
			for (String field : fields) {
				projectionList.add(Projections.property(field), field);
			}
			criteria.setProjection(projectionList);

			criteria.setResultTransformer(Transformers
					.aliasToBean(SeatEntity.class));
		}

		// Setting where clause
		if (statusFilter != null) {
			session.enableFilter("status").setParameterList("status",
					statusFilter);
		}

		// Setting Sorting fields
		if (sortFields != null) {
			for (String field : sortFields) {
				if (field.charAt(0) == SORT_BY_ORDER) {
					criteria.addOrder(Order.desc(field.substring(1)));
				} else {
					criteria.addOrder(Order.asc(field));
				}
			}
		}

		// Pagination
		criteria.setFirstResult(offSet * limit);
		criteria.setMaxResults(limit);

		return criteria.list();
	}

	@Override
	public String create(SeatEntity seat) {
		return sessionFactory.getCurrentSession().save(seat).toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.skein.ticket.persistent.dao.SeatDao#findById(int)
	 */
	@Override
	public SeatEntity findById(int seatId) {

		Property id = Property.forName("id");
		Session session = sessionFactory.getCurrentSession();

		Criteria criteria = session.createCriteria(SeatEntity.class);
		criteria.add(id.eq(seatId));

		// Tickets are not loaded by default as FetchMode.LAZY has been set.
		// While loading a single Seat, load the tickets as well. There are
		// three ways:
		// (1) Setting fetchMode to criteria
		// (2) By telling association in Hibernate.initialize
		// (3) Using Fetch Profiles

		// (1)
		// criteria.setFetchMode("tickets", FetchMode.JOIN);

		// (2)
		// Hibernate.initialize(seat.getTickets());

		// (3) Fetch Profiles

		SeatEntity seat = (SeatEntity) criteria.uniqueResult();
		// FIXME: seat name is not getting populated before updated the seat
		// name.
		return seat;
	}

	@Override
	public void update(SeatEntity seat) {
		sessionFactory.getCurrentSession().update(seat);
	}

	@Override
	public int delete(String seatId) {
		Session session = sessionFactory.getCurrentSession();
		int result = session.getNamedQuery(SeatEntity.QUERY_ID_DELETE_SEAT)
				.setString("seatId", seatId).executeUpdate();
		return result;
	}

	@Override
	public long getCount(List<String> statusFilter) {
		Session session = sessionFactory.getCurrentSession();

		// Setting where clause
		if (statusFilter != null) {
			session.enableFilter("status").setParameterList("status",
					statusFilter);
		}
		return (long) session.getNamedQuery(SeatEntity.QUERY_ID_GET_SEAT_COUNT).uniqueResult();
	}
}
