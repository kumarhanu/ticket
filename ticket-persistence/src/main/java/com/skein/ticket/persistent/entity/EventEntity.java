package com.skein.ticket.persistent.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.FetchProfile;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.SelectBeforeUpdate;

//@Cacheable
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@FetchProfile(name = EventEntity.PROFILE_EVENT_WITH_SEATS, fetchOverrides = { @FetchProfile.FetchOverride(entity = EventEntity.class, association = "seats", mode = FetchMode.JOIN) })
@NamedQueries({ @NamedQuery(name = EventEntity.QUERY_ID_DELETE_EVENT, query = EventEntity.QUERY_DELETE_EVENT) })
@NamedNativeQueries({ @NamedNativeQuery(name = EventEntity.QUERY_ID_EVENT_BY_TICKET, query = EventEntity.QUERY_EVENT_BY_TICKET, resultSetMapping = "eventByTicketRs") })
@SqlResultSetMapping(name = "eventByTicketRs", entities = { @EntityResult(entityClass = EventEntity.class) })
@DynamicUpdate
@SelectBeforeUpdate
@Entity
@Table(name = "EVENT")
public class EventEntity implements Serializable {

	private static final long serialVersionUID = 7851937943184195568L;

	public static final String QUERY_ID_DELETE_EVENT = "deleteEvent";
	static final String QUERY_DELETE_EVENT = "delete EventEntity where eventId = :eventId";

	public static final String QUERY_ID_EVENT_BY_TICKET = "selectEventByTicket";
	static final String QUERY_EVENT_BY_TICKET = "select * from EVENT event, TICKET ticket where event.event_id = ticket.event_id and ticket.ticket_id = :ticketId";

	public static final String PROFILE_EVENT_WITH_SEATS = "event-with-seats";

	private int eventId;
	private String eventName;
	private Date startTime;
	private Date endTime;
	private List<SeatEntity> seats;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "EVENT_ID", unique = true, nullable = false)
	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	@Column(name = "EVENT_NAME")
	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	/*
	 * For an Event, there are many Tickets will be issued.
	 * 
	 * One-to-Many uni-directional association.
	 * 
	 * Event Table doesn't hold any column for Ticket information. Ticket Table
	 * does hold EVENT_ID
	 * 
	 * mappedBy should the property name of the Event in the TicketEntity class.
	 * 
	 * Fetch the events lazily. While quering all events do not load seats.
	 * 
	 * Fetch seats when one Event is queried.
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "event", orphanRemoval = true, targetEntity = SeatEntity.class)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
	public List<SeatEntity> getSeats() {
		return seats;
	}

	public void setSeats(List<SeatEntity> seats) {
		this.seats = seats;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
}