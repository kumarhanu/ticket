package com.skein.ticket.persistent.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.skein.ticket.persistent.dao.ReservationDao;
import com.skein.ticket.persistent.entity.ReservationEntity;
import com.skein.ticket.persistent.entity.TicketEntity;

@Repository("reservationDao")
public class ReservationDaoImpl implements ReservationDao<ReservationEntity> {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<ReservationEntity> getAllReservations() {
		return sessionFactory.getCurrentSession()
				.createCriteria(ReservationEntity.class).list();
	}

	@Override
	public String create(ReservationEntity reservation) {
		return sessionFactory.getCurrentSession().save(reservation).toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.skein.ticket.persistent.dao.ReservationDao#findById(int)
	 */
	@Override
	public ReservationEntity findById(int reservationId) {

		Property id = Property.forName("id");
		Session session = sessionFactory.getCurrentSession();

		Criteria criteria = session.createCriteria(ReservationEntity.class);
		criteria.add(id.eq(reservationId));

		// Tickets are not loaded by default as FetchMode.LAZY has been set.
		// While loading a single Reservation, load the tickets as well. There
		// are
		// three ways:
		// (1) Setting fetchMode to criteria
		// (2) By telling association in Hibernate.initialize
		// (3) Using Fetch Profiles

		// (1)
		// criteria.setFetchMode("tickets", FetchMode.JOIN);

		// (2)
		// Hibernate.initialize(reservation.getTickets());

		// (3) Fetch Profiles

		ReservationEntity reservation = (ReservationEntity) criteria
				.uniqueResult();
		// FIXME: reservation name is not getting populated before updated the
		// reservation
		// name.
		return reservation;
	}

	@Override
	public void update(ReservationEntity reservation) {
		sessionFactory.getCurrentSession().update(reservation);
	}

}
