package com.skein.ticket.persistent.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "RESERVATION")
public class ReservationEntity {

	private int reservationId;
	private SeatEntity seat;
	private TicketEntity ticket;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "RESERVATION_ID")
	public int getReservationId() {
		return reservationId;
	}

	public void setReservationId(int reservationId) {
		this.reservationId = reservationId;
	}

	/*
	 * CascadeType.SAVE_UPDATE: Initially, Seat Status would be Not Allocated.
	 * When creating Ticket, the corresponding seat status should modify to
	 * 'ALLOCATED'
	 * 
	 * 	
	 * Reservation is the owner in Reservation-Seat Association
	 *
	 */
	@OneToOne(optional = false, fetch = FetchType.LAZY, orphanRemoval = true)
	@JoinColumn(name = "SEAT_ID")
	@OnDelete(action = OnDeleteAction.CASCADE)
	@Cascade({ org.hibernate.annotations.CascadeType.SAVE_UPDATE })
	public SeatEntity getSeat() {
		return seat;
	}

	public void setSeat(SeatEntity seat) {
		this.seat = seat;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TICKET_ID")
	public TicketEntity getTicket() {
		return ticket;
	}

	public void setTicket(TicketEntity ticket) {
		this.ticket = ticket;
	}

}
