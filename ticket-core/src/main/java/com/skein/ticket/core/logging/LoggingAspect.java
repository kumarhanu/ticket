package com.skein.ticket.core.logging;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;


@Aspect
public class LoggingAspect {

	@Around("execution(* com.skein.ticket..*.*(..))")
	public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {
		System.out.println("-------------------------------Before "+ joinPoint.toString() +" -------------------------------");
		Object value = joinPoint.proceed();
		System.out.println("-------------------------------After  "+ joinPoint.toString() +" -------------------------------");
		return value;
	}
}
