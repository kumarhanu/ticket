package com.skein.ticket.core.enums;

public enum SeatStatus {

	NOT_ALLOCATED("NA"), ALLOCATED("A");

	private String value = null;

	private SeatStatus(String value) {
		this.value = value;
	}

	public String toString() {
		return value;
	}
}
