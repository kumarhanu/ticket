package com.skein.ticket.bo;

import java.util.List;

import com.skein.ticket.domain.Ticket;

public interface TicketService<T extends Ticket> {

	public List<Ticket> getAllTickets();

	public String create(T ticket);

	public T findById(String ticketId);

	public void update(T ticket);

	public int delete(String ticketId);
}
