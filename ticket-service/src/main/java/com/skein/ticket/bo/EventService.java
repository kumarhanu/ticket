package com.skein.ticket.bo;

import java.util.List;

import com.skein.ticket.domain.Event;

public interface EventService<T extends Event> {

	public List<T> getAllEvents();

	public String create(T event);

	public T findById(String eventId);
	
	public T findByTicket(String ticketId);

	public void update(T event);

	public int delete(String eventId);
}
