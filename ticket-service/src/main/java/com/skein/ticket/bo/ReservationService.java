package com.skein.ticket.bo;

import java.util.List;

import com.skein.ticket.domain.Reservation;

public interface ReservationService<T extends Reservation> {

	public List<T> getAllReservations();

	public String create(T reservation);

	public T findById(String reservationId);

	public void update(T reservation);
}
