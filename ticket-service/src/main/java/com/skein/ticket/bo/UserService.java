package com.skein.ticket.bo;

import java.util.List;

import com.skein.ticket.domain.User;

public interface UserService<T extends User> {

	public List<User> getAllUsers();

	public String create(T user);

	public T findById(String userId);

	public void update(T user);

	public int delete(String userId);
}
