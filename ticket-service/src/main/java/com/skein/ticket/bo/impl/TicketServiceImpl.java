package com.skein.ticket.bo.impl;

import java.util.List;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.skein.ticket.bo.TicketService;
import com.skein.ticket.core.enums.SeatStatus;
import com.skein.ticket.domain.Event;
import com.skein.ticket.domain.Reservation;
import com.skein.ticket.domain.Seat;
import com.skein.ticket.domain.Ticket;
import com.skein.ticket.domain.User;
import com.skein.ticket.persistent.dao.TicketDao;
import com.skein.ticket.persistent.entity.EventEntity;
import com.skein.ticket.persistent.entity.ReservationEntity;
import com.skein.ticket.persistent.entity.SeatEntity;
import com.skein.ticket.persistent.entity.TicketEntity;
import com.skein.ticket.persistent.entity.UserEntity;

@Service("ticketService")
public class TicketServiceImpl implements TicketService<Ticket> {

	@Autowired
	TicketDao<com.skein.ticket.persistent.entity.TicketEntity> ticketDao;
	
	@Transactional
	@Override
	public List<Ticket> getAllTickets() {
		List<TicketEntity> tickets = this.ticketDao.getAllTickets();

		// START: Mapping
		ConfigurableMapper mapper = new ConfigurableMapper() {
			@Override
			protected void configure(MapperFactory factory) {

				factory.classMap(TicketEntity.class, Ticket.class).byDefault()
						.register();

				factory.classMap(ReservationEntity.class, Reservation.class)
						.exclude("ticket").byDefault().register();

				factory.classMap(SeatEntity.class, Seat.class).exclude("event")
						.byDefault().register();

				factory.classMap(EventEntity.class, Event.class)
						.exclude("tickets").byDefault().register();

				factory.classMap(UserEntity.class, User.class)
						.exclude("tickets").byDefault().register();
			}
		};
		// END: Mapping

		List<Ticket> ticketDomains = mapper.mapAsList(tickets, Ticket.class);
		return ticketDomains;
	}

	@Transactional
	public String create(Ticket ticket) {

		// START: Mapping
		ConfigurableMapper mapper = new ConfigurableMapper() {
			@Override
			protected void configure(MapperFactory factory) {

				factory.classMap(Ticket.class, TicketEntity.class).byDefault()
						.register();
			}
		};
		// END: Mapping
		TicketEntity ticketEntity = mapper.map(ticket, TicketEntity.class);

		// This is required to Cascade.Save for ReservationEntity
		for (ReservationEntity reservation : ticketEntity.getReservations()) {
			reservation.setTicket(ticketEntity);
			reservation.getSeat().setStatus(SeatStatus.ALLOCATED);
			reservation.getSeat().setEvent(ticketEntity.getEvent());
			System.out.println(reservation.getSeat().getEvent());
		}

		return this.ticketDao.create(ticketEntity);
	}

	@Transactional
	public Ticket findById(String ticketId) {

		TicketEntity ticket = this.ticketDao.findById(Integer
				.parseInt(ticketId));
		// START: Mapping
		ConfigurableMapper mapper = new ConfigurableMapper() {
			@Override
			protected void configure(MapperFactory factory) {

				factory.classMap(ReservationEntity.class, Reservation.class)
						.exclude("ticket").byDefault().register();

				factory.classMap(SeatEntity.class, Seat.class).exclude("event")
						.byDefault().register();

				factory.classMap(EventEntity.class, Event.class)
						.exclude("tickets").byDefault().register();

				factory.classMap(UserEntity.class, User.class)
						.exclude("tickets").byDefault().register();
			}
		};
		// END: Mapping

		Ticket ticketDomain = mapper.map(ticket, Ticket.class);
		return ticketDomain;
	}

	@Transactional
	public void update(Ticket ticket) {
		// START: Mapping
		ConfigurableMapper mapper = new ConfigurableMapper() {
			@Override
			protected void configure(MapperFactory factory) {

				factory.classMap(Ticket.class, TicketEntity.class).byDefault()
						.register();
			}
		};
		// END: Mapping
		this.ticketDao.update(mapper.map(ticket, TicketEntity.class));
	}

	@Transactional
	public int delete(String ticketId) {
		return this.ticketDao.delete(ticketId);

	}
}