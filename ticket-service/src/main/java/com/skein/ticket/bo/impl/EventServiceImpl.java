package com.skein.ticket.bo.impl;

import java.util.List;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.skein.ticket.bo.EventService;
import com.skein.ticket.domain.Event;
import com.skein.ticket.domain.Seat;
import com.skein.ticket.persistent.dao.EventDao;
import com.skein.ticket.persistent.dao.SeatDao;
import com.skein.ticket.persistent.entity.EventEntity;
import com.skein.ticket.persistent.entity.SeatEntity;

@Service("eventService")
public class EventServiceImpl implements EventService<Event> {

	@Autowired
	EventDao<EventEntity> eventDao;

	@Transactional
	@Override
	public List<Event> getAllEvents() {
		List<EventEntity> events = this.eventDao.getAllEvents();

		// START: Mapping
		ConfigurableMapper mapper = new ConfigurableMapper() {
			@Override
			protected void configure(MapperFactory factory) {

				factory.classMap(EventEntity.class, Event.class)
						.exclude("seats").byDefault().register();
			}
		};
		// END: Mapping

		List<Event> eventDomains = mapper.mapAsList(events, Event.class);
		return eventDomains;
	}

	@Transactional
	@Override
	public String create(Event event) {
		// START: Mapping
		ConfigurableMapper mapper = new ConfigurableMapper() {
			@Override
			protected void configure(MapperFactory factory) {
				factory.classMap(Event.class, EventEntity.class).byDefault()
						.register();

			}
		};
		// END: Mapping

		return this.eventDao.create(mapper.map(event, EventEntity.class));
	}

	@Transactional
	@Override
	public Event findById(String eventId) {
		EventEntity event = this.eventDao.findById(Integer.parseInt(eventId));

		// START: Mapping
		ConfigurableMapper mapper = new ConfigurableMapper() {
			@Override
			protected void configure(MapperFactory factory) {
				factory.classMap(EventEntity.class, Event.class).byDefault()
						.register();
				factory.classMap(SeatEntity.class, Seat.class).exclude("event")
						.byDefault().register();
			}
		};
		// END: Mapping

		Event eventDomain = mapper.map(event, Event.class);
		return eventDomain;
	}

	@Transactional
	@Override
	public Event findByTicket(String ticketId){
		EventEntity event = this.eventDao.findByTicket(ticketId);
		
		// START: Mapping
		ConfigurableMapper mapper = new ConfigurableMapper() {
			@Override
			protected void configure(MapperFactory factory) {
				factory.classMap(EventEntity.class, Event.class).byDefault()
						.register();
				factory.classMap(SeatEntity.class, Seat.class).exclude("event")
						.byDefault().register();
			}
		};
		// END: Mapping

		Event eventDomain = mapper.map(event, Event.class);
		return eventDomain;		
	}
	
	@Transactional
	@Override
	public void update(Event event) {

		// START: Mapping
		ConfigurableMapper mapper = new ConfigurableMapper() {
			@Override
			protected void configure(MapperFactory factory) {
				factory.classMap(Event.class, EventEntity.class).byDefault()
						.register();
			}
		};
		// END: Mapping

		EventEntity eventEntity = mapper.map(event, EventEntity.class);
		eventDao.update(eventEntity);
	}

	@Transactional
	@Override
	public int delete(String eventId) {
		return eventDao.delete(eventId);
	}
}