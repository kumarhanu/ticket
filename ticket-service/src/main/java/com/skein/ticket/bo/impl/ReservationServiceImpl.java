package com.skein.ticket.bo.impl;

import java.util.List;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.skein.ticket.bo.ReservationService;
import com.skein.ticket.domain.Reservation;
import com.skein.ticket.domain.Ticket;
import com.skein.ticket.domain.User;
import com.skein.ticket.persistent.dao.ReservationDao;
import com.skein.ticket.persistent.entity.ReservationEntity;
import com.skein.ticket.persistent.entity.TicketEntity;
import com.skein.ticket.persistent.entity.UserEntity;

@Service("reservationService")
public class ReservationServiceImpl implements ReservationService<Reservation> {

	@Autowired
	ReservationDao<ReservationEntity> reservationDao;

	@Transactional
	@Override
	public List<Reservation> getAllReservations() {
		List<ReservationEntity> reservations = this.reservationDao.getAllReservations();
		
		//START: Mapping
		ConfigurableMapper mapper = new ConfigurableMapper() {
			@Override
			protected void configure(MapperFactory factory) {

				factory.classMap(ReservationEntity.class, Reservation.class)
						.exclude("tickets").byDefault().register();
			}
		};
		//END: Mapping

		List<Reservation> reservationDomains = mapper.mapAsList(reservations, Reservation.class);
		return reservationDomains;
	}

	@Transactional
	@Override
	public String create(Reservation reservation) {
		//START: Mapping		
		ConfigurableMapper mapper = new ConfigurableMapper() {
			@Override
			protected void configure(MapperFactory factory) {
				factory.classMap(Reservation.class, ReservationEntity.class).byDefault()
						.register();

			}
		};
		//END: Mapping

		return this.reservationDao.create(mapper.map(reservation, ReservationEntity.class));
	}

	@Transactional
	@Override
	public Reservation findById(String reservationId) {
		ReservationEntity reservation = this.reservationDao.findById(Integer.parseInt(reservationId));
		
		//START: Mapping
		ConfigurableMapper mapper = new ConfigurableMapper() {
			@Override
			protected void configure(MapperFactory factory) {
				factory.classMap(ReservationEntity.class, Reservation.class).byDefault()
						.register();
				factory.classMap(TicketEntity.class, Ticket.class)
						.exclude("reservation").byDefault().register();
				factory.classMap(UserEntity.class, User.class)
						.exclude("tickets").byDefault().register();
			}
		};
		//END: Mapping

		Reservation reservationDomain = mapper.map(reservation, Reservation.class);
		return reservationDomain;
	}

	@Transactional
	@Override
	public void update(Reservation reservation) {
		
		//START: Mapping
		ConfigurableMapper mapper = new ConfigurableMapper() {
			@Override
			protected void configure(MapperFactory factory) {
				factory.classMap(Reservation.class, ReservationEntity.class).byDefault()
						.register();
			}
		};
		//END: Mapping

		ReservationEntity reservationEntity = mapper.map(reservation, ReservationEntity.class);
		reservationDao.update(reservationEntity);
	}
}