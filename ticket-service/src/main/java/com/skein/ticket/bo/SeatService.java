package com.skein.ticket.bo;

import java.util.List;

import com.skein.ticket.domain.Seat;

public interface SeatService<T extends Seat> {

	public List<T> getAllSeats(List<String> fields, List<String> statusFilter,
			List<String> sortFields, int offSet, int limit);

	public String create(T seat);

	public T findById(String seatId);

	public void update(T seat);

	public int delete(String seatId);
	
	public long getCount(List<String> statusFilter);
}
