package com.skein.ticket.bo.impl;

import java.util.List;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.skein.ticket.bo.SeatService;
import com.skein.ticket.domain.Event;
import com.skein.ticket.domain.Seat;
import com.skein.ticket.domain.Ticket;
import com.skein.ticket.domain.User;
import com.skein.ticket.persistent.dao.SeatDao;
import com.skein.ticket.persistent.entity.EventEntity;
import com.skein.ticket.persistent.entity.SeatEntity;
import com.skein.ticket.persistent.entity.TicketEntity;
import com.skein.ticket.persistent.entity.UserEntity;

@Service("seatService")
public class SeatServiceImpl implements SeatService<Seat> {

	@Autowired
	SeatDao<SeatEntity> seatDao;

	@Transactional
	@Override
	public List<Seat> getAllSeats(List<String> fields,
			List<String> statusFilter, List<String> sortFields, int offSet,
			int limit) {

		List<SeatEntity> seats = this.seatDao.getAllSeats(fields, statusFilter,
				sortFields, offSet, limit);

		// START: Mapping
		ConfigurableMapper mapper = new ConfigurableMapper() {
			@Override
			protected void configure(MapperFactory factory) {

				factory.classMap(SeatEntity.class, Seat.class).byDefault()
						.register();
				factory.classMap(EventEntity.class, Event.class)
						.exclude("seats").byDefault().register();
			}
		};
		// END: Mapping

		List<Seat> seatDomains = mapper.mapAsList(seats, Seat.class);
		return seatDomains;
	}

	@Transactional
	@Override
	public String create(Seat seat) {
		// START: Mapping
		ConfigurableMapper mapper = new ConfigurableMapper() {
			@Override
			protected void configure(MapperFactory factory) {
				factory.classMap(Seat.class, SeatEntity.class).byDefault()
						.register();
			}
		};
		// END: Mapping

		return this.seatDao.create(mapper.map(seat, SeatEntity.class));
	}

	@Transactional
	@Override
	public Seat findById(String seatId) {
		SeatEntity seat = this.seatDao.findById(Integer.parseInt(seatId));

		// START: Mapping
		ConfigurableMapper mapper = new ConfigurableMapper() {
			@Override
			protected void configure(MapperFactory factory) {
				factory.classMap(SeatEntity.class, Seat.class).byDefault()
						.register();
				factory.classMap(EventEntity.class, Event.class)
						.exclude("seats").byDefault().register();
			}
		};
		// END: Mapping

		Seat seatDomain = mapper.map(seat, Seat.class);
		return seatDomain;
	}

	@Transactional
	@Override
	public void update(Seat seat) {

		// START: Mapping
		ConfigurableMapper mapper = new ConfigurableMapper() {
			@Override
			protected void configure(MapperFactory factory) {
				factory.classMap(Seat.class, SeatEntity.class).byDefault()
						.register();
			}
		};
		// END: Mapping

		SeatEntity seatEntity = mapper.map(seat, SeatEntity.class);
		seatDao.update(seatEntity);
	}

	@Transactional
	@Override
	public int delete(String seatId) {
		return seatDao.delete(seatId);
	}
	
	@Transactional
	@Override
	public long getCount(List<String> statusFilter){
		return seatDao.getCount(statusFilter);
	}
}