package com.skein.ticket.bo.impl;

import java.util.List;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.skein.ticket.bo.UserService;
import com.skein.ticket.domain.Event;
import com.skein.ticket.domain.Reservation;
import com.skein.ticket.domain.Seat;
import com.skein.ticket.domain.Ticket;
import com.skein.ticket.domain.User;
import com.skein.ticket.persistent.dao.UserDao;
import com.skein.ticket.persistent.entity.EventEntity;
import com.skein.ticket.persistent.entity.ReservationEntity;
import com.skein.ticket.persistent.entity.SeatEntity;
import com.skein.ticket.persistent.entity.TicketEntity;
import com.skein.ticket.persistent.entity.UserEntity;

@Service("userService")
public class UserServiceImpl implements UserService<User> {

	@Autowired
	UserDao<com.skein.ticket.persistent.entity.UserEntity> userDao;

	@Transactional
	@Override
	public List<User> getAllUsers() {
		List<UserEntity> users = this.userDao.getAllUsers();

		// START: Mapping
		ConfigurableMapper mapper = new ConfigurableMapper() {
			@Override
			protected void configure(MapperFactory factory) {

				factory.classMap(UserEntity.class, User.class)
						.exclude("tickets").byDefault().register();
			}
		};
		// END: Mapping

		return mapper.mapAsList(users, User.class);
	}

	@Transactional
	public String create(User user) {

		// START: Mapping
		ConfigurableMapper mapper = new ConfigurableMapper() {
			@Override
			protected void configure(MapperFactory factory) {

				factory.classMap(User.class, UserEntity.class).byDefault()
						.register();
			}
		};
		// END: Mapping

		return this.userDao.create(mapper.map(user, UserEntity.class));
	}

	@Transactional
	public User findById(String userId) {
		// START: Mapping
		ConfigurableMapper mapper = new ConfigurableMapper() {
			@Override
			protected void configure(MapperFactory factory) {
				factory.classMap(UserEntity.class, User.class).byDefault()
						.register();
				factory.classMap(ReservationEntity.class, Reservation.class)
						.exclude("ticket").byDefault().register();
				factory.classMap(SeatEntity.class, Seat.class).exclude("event")
						.byDefault().register();
				factory.classMap(EventEntity.class, Event.class)
						.exclude("tickets").byDefault().register();
				factory.classMap(TicketEntity.class, Ticket.class)
						.exclude("user").byDefault().register();
			}
		};
		// END: Mapping

		return mapper.map(this.userDao.findById(Integer.parseInt(userId)),
				User.class);
	}

	@Transactional
	public void update(User user) {
		// START: Mapping
		ConfigurableMapper mapper = new ConfigurableMapper() {
			@Override
			protected void configure(MapperFactory factory) {

				factory.classMap(User.class, UserEntity.class).byDefault()
						.register();
			}
		};
		// END: Mapping

		this.userDao.update(mapper.map(user, UserEntity.class));
	}

	@Transactional
	public int delete(String userId) {
		return this.userDao.delete(userId);
	}
}
