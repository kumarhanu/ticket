package com.skein.ticket.couchbase;

import java.util.Collection;
import java.util.List;
import java.util.Map;


public interface UserProfileService {
	
	public String create(String userProfileDocument);
	
	public Object get(String userProfileId);
	
	public Collection<Object> getAll(List<String> fields, List<String> fbIdFilter,
			List<String> sortFields, int offSet, int limit);

	public int getCount(Map<String, String> filter);
	
}