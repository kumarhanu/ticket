package com.skein.ticket.couchbase;

import java.util.Collection;

public interface UserSessionService {
	public String create(String userId, String userProfileDocument);
	public Object get(String userProfileId);
	public Collection<Object> getAll(String userProfileId);

}