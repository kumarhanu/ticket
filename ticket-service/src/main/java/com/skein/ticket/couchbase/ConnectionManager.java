package com.skein.ticket.couchbase;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.couchbase.client.CouchbaseClient;
import com.couchbase.client.protocol.views.DesignDocument;
import com.couchbase.client.protocol.views.ViewDesign;

public class ConnectionManager implements ServletContextListener {

	  /**
	   * Holds the connected Couchbase instance.
	   */
	  private static CouchbaseClient client;

	  private DesignDocument getDesignDocument(String name) {
	    try {
	        return client.getDesignDoc(name);
	    } catch (com.couchbase.client.protocol.views.InvalidViewException e) {
	        return new DesignDocument(name);
	    }
	  }

	  /**
	   * Connect to Couchbase when the Server starts.
	   *
	   * @param sce the ServletContextEvent (not used here).
	   */
	  @Override
	  public void contextInitialized(ServletContextEvent sce) {
	    String cluster = System.getProperty("com.couchbase.beersample.cluster", "http://127.0.0.1:8091/pools");
	    ArrayList<URI> nodes = new ArrayList<URI>();
	    nodes.add(URI.create(cluster));
	    try {
	      client = new CouchbaseClient(nodes, "ticket", "password");
	    } catch (IOException ex) {
	    }
	    
	    System.out.println("------------------------- "+client);

	    try {
	        DesignDocument designDoc = getDesignDocument("userprofile");
	        boolean found = false;
	        for (ViewDesign view : designDoc.getViews()) {
	            if (view.getMap() == "by_name") {
	                found = true;
	                break;
	            }
	        }

	        if (!found) {
	            ViewDesign design = new ViewDesign("by_name", "function (doc, meta) {\n" +
	                    "  if(doc.type && doc.type == \"beer\") {\n" +
	                    "    emit(doc.name, null);\n" +
	                    "  }\n" +
	                    "}");
	            designDoc.getViews().add(design);
	            client.createDesignDoc(designDoc);
	        }
	    } catch (Exception e) {
	    }
	    System.out.println("------------------------- "+client);
	  }

	  /**
	   * Disconnect from Couchbase when the Server shuts down.
	   *
	   * @param sce the ServletContextEvent (not used here).
	   */
	  @Override
	  public void contextDestroyed(ServletContextEvent sce) {
		  System.out.println("contextDestroyed called");
	    client.shutdown();
	  }

	  /**
	   * Returns the current CouchbaseClient object.
	   *
	   * @return the current Couchbase connection.
	   */
	  public static CouchbaseClient getInstance() {
	    return client;
	  }
}