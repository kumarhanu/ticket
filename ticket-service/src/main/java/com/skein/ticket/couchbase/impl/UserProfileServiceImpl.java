package com.skein.ticket.couchbase.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.stereotype.Service;

import com.couchbase.client.CouchbaseClient;
import com.couchbase.client.protocol.views.ComplexKey;
import com.couchbase.client.protocol.views.Query;
import com.couchbase.client.protocol.views.View;
import com.couchbase.client.protocol.views.ViewResponse;
import com.couchbase.client.protocol.views.ViewRow;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.skein.ticket.couchbase.ConnectionManager;
import com.skein.ticket.couchbase.IDGenerator;
import com.skein.ticket.couchbase.UserProfileService;

@Service
public class UserProfileServiceImpl implements UserProfileService {

	private static final String USER_ID = "user_id";
	private static final String USER_PROFILE_TYPE = "userprofile";
	private static final String USER_KEY_TYPE = "userkey";

	@Override
	public String create(String userProfileDocument) {
		CouchbaseClient client = ConnectionManager.getInstance();
		Gson gson = new Gson();

		System.out.println(client);
		System.out.println(userProfileDocument);

		String userId = IDGenerator.generateId(client, USER_ID);

		JsonElement userProfile = gson.fromJson(userProfileDocument,
				JsonElement.class);

		JsonElement userKey = userProfile.getAsJsonObject().get("key");
		JsonObject userJson = userKey.getAsJsonObject();
		userJson.addProperty(USER_ID, userId);
		userJson.addProperty("type", USER_KEY_TYPE);

		JsonObject userProfileJson = userProfile.getAsJsonObject();
		userProfileJson.addProperty(USER_ID, userId);
		userProfileJson.addProperty("type", USER_PROFILE_TYPE);
		userProfileJson.remove("key");

		System.out.println(userJson);
		client.add(USER_KEY_TYPE + "_" + userId, 0, gson.toJson(userJson));
		client.add(USER_PROFILE_TYPE + "_" + userId, 0,
				gson.toJson(userProfileJson));

		return userId;
	}

	@Override
	public Object get(String userProfileId) {
		CouchbaseClient client = ConnectionManager.getInstance();
		String userProfileJson = (String) client.get(USER_PROFILE_TYPE + "_"
				+ userProfileId);
		String userKeyJson = (String) client.get(USER_KEY_TYPE + "_"
				+ userProfileId);

		Gson gson = new Gson();
		JsonObject result = gson.fromJson(userProfileJson, JsonElement.class)
				.getAsJsonObject();
		result.add("key", gson.fromJson(userKeyJson, JsonElement.class));
		return gson.toJson(result);
	}

	@Override
	public Collection<Object> getAll(List<String> fields, List<String> fbIdFilter,
			List<String> sortFields, int offSet, int limit) {

		
		Client client = new TransportClient()
				.addTransportAddress(new InetSocketTransportAddress(
						"localhost", 9300));
		
		
		QueryBuilder qb = QueryBuilders.boolQuery().must(QueryBuilders.matchQuery("fbid", fbIdFilter.get(0)));
		
		SearchResponse response = client.prepareSearch("ticket").setTypes("couchbaseDocument").setQuery(qb).setFrom(offSet).setSize(limit).addSort(sortFields.get(0), SortOrder.ASC).addSort(sortFields.get(1), SortOrder.DESC).execute().actionGet();
		
		SearchHit[] results = response.getHits().getHits();
		
		Gson gson = new Gson();
		//JsonArray users = new JsonArray();
		List<String> users = new ArrayList<>();
		
		for(SearchHit hit: results){
			System.out.println("******* "+hit.getSource());
			System.out.println("******* "+hit.getId());
			
			//users.add(gson.fromJson(hit.getId(), JsonElement.class));
			users.add(hit.getId());
		}
		
		client.close();
		
		CouchbaseClient cbClient = ConnectionManager.getInstance();
		
		Map<String,Object>  usersList = cbClient.getBulk(users);
		System.out.println(usersList);
		
		return usersList.values();
	}

	@Override
	public int getCount(Map<String, String> filter) {
		CouchbaseClient client = ConnectionManager.getInstance();
		View view = client.getView("ticket", "UserCountByPhone");
		Query query = new Query();
		query.setReduce(true);
		query.setGroup(true);
		// query.setKeys("[ \"(508)826-4892\", \"(508)826-4893\"]");
		// query.setKey("(508)826-4892");
		String phoneNo = filter.get("phoneno");
		String email = filter.get("email");
		ComplexKey ck1 = ComplexKey.of(email, phoneNo);
		query.setKey(ck1);

		// query.setKey("{\"email\" : \"kumar@gmail.com\",\"phone\" : \"(508)826-4892\"}");

		System.out.println(view);
		ViewResponse response = client.query(view, query);

		int result = 0;
		for (ViewRow row : response) {
			System.out.println(row.getKey());
			System.out.println(row.getValue());
			result = Integer.parseInt(row.getValue());
		}
		return result;
	}
}
