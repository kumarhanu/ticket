package com.skein.ticket.couchbase;

import java.util.UUID;

import com.couchbase.client.CouchbaseClient;

public class IDGenerator {

	private static final int INCREMENT_BY = 1;
	private static final long DEFAULT_ID_VALUE = 100;  
	
	public static String generateId(CouchbaseClient client, String key ){
		//long newId = client.incr(key, INCREMENT_BY, DEFAULT_ID_VALUE);
		
		return key + "_" + UUID.randomUUID(); 
	}
}
