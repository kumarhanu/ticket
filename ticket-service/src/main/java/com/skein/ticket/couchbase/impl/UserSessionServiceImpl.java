package com.skein.ticket.couchbase.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import net.spy.memcached.CASResponse;
import net.spy.memcached.CASValue;

import org.springframework.stereotype.Service;

import com.couchbase.client.CouchbaseClient;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.skein.ticket.couchbase.ConnectionManager;
import com.skein.ticket.couchbase.IDGenerator;
import com.skein.ticket.couchbase.UserSessionService;

@Service
public class UserSessionServiceImpl implements UserSessionService {

	private static final String USER_SESSION_ID = "session_id";
	private static final String USER_SESSION_TYPE = "usersession";
	private static final String USER_PROFILE_TYPE = "userprofile";

	@Override
	public String create(String userId, String userSessionDocument) {
		CouchbaseClient client = ConnectionManager.getInstance();
		Gson gson = new Gson();

		System.out.println(client);
		System.out.println(userSessionDocument);

		JsonElement userSession = gson.fromJson(userSessionDocument,
				JsonElement.class);

		JsonObject userSessionJson = userSession.getAsJsonObject();
		String sessionId = IDGenerator.generateId(client, USER_SESSION_ID);
		userSessionJson.addProperty(USER_SESSION_ID, sessionId);
		userSessionJson.addProperty("type", USER_SESSION_TYPE);

		CASValue<Object> cas = client.gets(USER_PROFILE_TYPE + "_" + userId);
		System.out.println("*** " + cas.getValue());
		System.out.println("*** " + cas.getCas());

		// Add session_id to User Document; Since there is not option to append
		// to the existing JSON document from Couchbase, First get the User
		// Document -> Get the Sessions property and add
		// Session Id to it.

		// JsonElement userDocument =
		// gson.fromJson((String)client.get(USER_PROFILE_TYPE + "_" +
		// userId),JsonElement.class);
		JsonElement userDocument = gson.fromJson((String) cas.getValue(),
				JsonElement.class);
		System.out.println(userDocument);
		JsonArray sessionElement = userDocument.getAsJsonObject()
				.get("sessions").getAsJsonArray();
		System.out.println(sessionElement);

		sessionElement.add(gson.fromJson(sessionId, JsonElement.class));
		// client.replace(USER_PROFILE_TYPE + "_" + userId,
		// gson.toJson(userDocument));

		CASResponse response = client.cas(USER_PROFILE_TYPE + "_" + userId,
				cas.getCas(), gson.toJson(userDocument));
		System.out.println(response.toString());

		// Create Session Document
		client.add(USER_SESSION_TYPE + "_" + sessionId, 2 * 60,
				gson.toJson(userSessionJson));

		return userId;
	}

	@Override
	public Object get(String userSessionId) {
		CouchbaseClient client = ConnectionManager.getInstance();
		String userSessionJson = (String) client.get(USER_SESSION_TYPE + "_"
				+ userSessionId);

		Gson gson = new Gson();
		JsonObject result = gson.fromJson(userSessionJson, JsonElement.class)
				.getAsJsonObject();
		return gson.toJson(result);
	}

	//FIXME:
	@Override
	public Collection<Object> getAll(String userProfileId) {

		CouchbaseClient client = ConnectionManager.getInstance();
		Gson gson = new Gson();
		
		JsonElement userDocument = gson.fromJson(
				(String)client.get(USER_PROFILE_TYPE + "_" + userProfileId),
				JsonElement.class);
		
		JsonArray sessions = userDocument.getAsJsonObject().get("sessions").getAsJsonArray();
		
		System.out.println(sessions);
		List<String> list = new ArrayList<>();
		for (int i=0; i<sessions.size(); i++) {
			System.out.println(sessions.get(i).toString());
		    list.add(USER_SESSION_TYPE + "_" + gson.toJson(sessions.get(i)) );
		}
		for (int i=0; i<sessions.size(); i++) {
			System.out.println(list.get(i));
		}
		Map<String,Object>  sessionList = client.getBulk(list);
		System.out.println(sessionList);
		return (Collection<Object>)sessionList.values();
	}
}
