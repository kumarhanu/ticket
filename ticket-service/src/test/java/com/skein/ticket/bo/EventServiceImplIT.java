package com.skein.ticket.bo;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

import java.sql.SQLException;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.skein.ticket.bo.config.TestContext;
import com.skein.ticket.domain.Event;

/**
 * Integration test for EventServiceImpl.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestContext.class })
@ActiveProfiles(profiles = "dev")
public class EventServiceImplIT {
	@Autowired
	private HibernateTransactionManager manager;

	@Autowired
	private EventService<Event> eventService;

	private Event event;

	@Before
	public void setUp() {
		event = new Event();
		event.setEventName("testeventname");
	}

	@After
	public void tearDown() {
		event = null;
	}

	@Test
	public void testGetAllEvents() throws SQLException {

		String eventId = eventService.create(event);
		assertThat(eventId, is(notNullValue()));

		List<Event> events = eventService.getAllEvents();
		assertThat(events.size(), is(1));
		assertThat(
				events.get(0),
				allOf(hasProperty("eventId", is("1")),
						hasProperty("eventName", is("testeventname"))));
	}

}
