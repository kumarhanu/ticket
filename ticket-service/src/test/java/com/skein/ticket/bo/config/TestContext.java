package com.skein.ticket.bo.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;

import com.skein.ticket.persistent.config.HSQLDBPersistenceConfig;

@Profile("dev")
@Configuration
@Import(HSQLDBPersistenceConfig.class)
@ComponentScan({ "com.skein.ticket.bo.*"})
public class TestContext {

}
