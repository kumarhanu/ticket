# Ticket: ![Lacey_Street_Theatre_ticket_194359.jpg](https://bitbucket.org/repo/qAB7rK/images/1667870653-Lacey_Street_Theatre_ticket_194359.jpg)

This application is primarily facilitates online registration for an Event.

High level business flows:

* Admin will create Event and Seats information.
* User enrolled to Ticket application. Enrollment to app is optional. User can use the app as guest also.
* User reserves Seat(s).
* User access, modify, delete the tickets that they have created.
* User's session/profile has been stored in NoSQL/Distributed Data Base which gives low latency, scalable way of accessing profile information through web and various smart devices.
* RESTful based APIs are exposed to manage various resources. Plan to expose same functionality as Messaging (JMS) interface.

# Sequence Diagram for Ticket work flow:
![ticket.png](https://bitbucket.org/repo/qAB7rK/images/3573590934-ticket.png)

|Artifact               | Framework           |
|---------------------- | :-------------------|
|User Interface         | Angular.js                |
|RESTful Services                    | RestEasy            |
|RESTful API Documentation | Swagger |
|REST client            | Postman |
|Dependency Injection   | Spring|
|Object Mapper          | Orika               |
|Persistence            | Hibernate|
|Second level cache     | Memcache|
|RDBMS                  | HSQLDB(In Memory)|
|Distributed Data Store (NoSQL)    | Couchbase|
|Search Engine          | ElasticSearch|
|Integration            | Spring - Yet to implement|
|JSON-Object            | Jackson & gson|
|Distributed Computing  | Hadoop - Yet to implement|
|Analytics              | Splunk - Yet to implement |
|Bean validation    | Hibernate Validator - Yet to implement  |
|Web server             | Embedded Tomcat|
|Build                  | Maven|
|Continuous Integration | Jenkins|
|Continuous Delivery     | Yet to implement|
|Configuration Mgmt.    | BitBucket|
|Testing                                     |
|   Unit Testing        | JUnit, Hamcrest, Spring Test|
|   Integration Testing | JUnit, Hamcrest, Spring Test|
|   Mock                   | Mockito|
|Security               | OAuth |
|Static Code analysis   | CheckStyle (Conventions), PMD (Bad practices), FindBugs (Potential bugs), Maker (Architecture issues)-Yet to implement |
|Monitoring             | DynaTrace - Yet to implement|
|Logging                | SL4J/Log4J|
|IDE                    | Spring Tool Suite|
|UML                    | PlantUML|
|Profiler               | YourKit |
|Messaging| ActiveMQ - Yet to implement|

## Useful Commands

Run application

    ./run.sh
    
####Maven:

Pointing to a local settings.xml

    mvn -s ~/.m2/settings.xml.TICKET

Run Unit Test

    mvn -s ~/.m2/settings.xml.TICKET test
    
Run Integration Tests

    mvn -s ~/.m2/settings.xml.TICKET integration-test
    
Run Integration Tests and skip Unit Tests

    mvn -s ~/.m2/settings.xml.TICKET integration-test -Dskip.surefire.tests=true
    
Run Sonar

    mvn -s ~/.m2/settings.xml.TICKET sonar:sonar
    
Run checkstyle

    mvn -s ~/.m2/settings.xml.TICKET checkstyle:checkstyle

Run PlantUML: Regenerates UML diagrams in .gif reading from .puml files. 

    mvn -s ~/.m2/settings.xml.TICKET clean com.github.jeluard:plantuml-maven-plugin:generate

####MySQL:

    $ sudo $MYSQL_HOME/support-files/mysql.server start
    $ sudo $MYSQL_HOME/support-files/mysql.server restart
    $ sudo $MYSQL_HOME/support-files/mysql.server stop

    $ mysql -u root -p

####Sonar: (admin/admin) (sonar/sonar) localhost:9000

    $ sudo $SONAR_HOME/bin/macosx-universal-64/sonar.sh start
    $ sudo $SONAR_HOME/bin/macosx-universal-64/sonar.sh restart
    $ sudo $SONAR_HOME/bin/macosx-universal-64/sonar.sh stop

####Couchbase:

    /Users/USER_NAME/Library/Application Support/Couchbase
        
####ElasticSearch:

####Hadoop:

####Jenkins:
    
    localhost:8080/restart
        
## Useful Links

SonarQube : (http://localhost:9000/) (admin/admin)

Jenkins       : (http://localhost:8080/)

Postman     : (chrome-extension://fdmmgilgnpjigdojojpjoooidkmcomcm/index.html)

Couchbase : (http://127.0.0.1:8091/index.html) (Administrator/password)


## ToDo

- Publish Code coverage from Unit and Integration Test into Sonar