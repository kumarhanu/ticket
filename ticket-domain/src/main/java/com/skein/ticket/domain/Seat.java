package com.skein.ticket.domain;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.skein.ticket.core.enums.SeatStatus;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Seat {

	private int seatId;
	private String seatNumber;
	private SeatStatus status = SeatStatus.NOT_ALLOCATED;
	private Event event;

	public int getSeatId() {
		return seatId;
	}

	public void setSeatId(int seatId) {
		this.seatId = seatId;
	}

	public String getSeatNumber() {
		return seatNumber;
	}

	public void setSeatNumber(String seatNumber) {
		this.seatNumber = seatNumber;
	}

	public SeatStatus getStatus() {
		return status;
	}

	public void setStatus(SeatStatus status) {
		this.status = status;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(" seatId ");
		sb.append(seatId);
		sb.append(" seatNumber ");
		sb.append(seatNumber);
		sb.append(" status ");
		sb.append(status);
		return sb.toString();
	}
}
