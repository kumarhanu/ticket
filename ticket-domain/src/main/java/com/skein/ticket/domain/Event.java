package com.skein.ticket.domain;

import java.util.Date;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

@ApiModel
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Event {

	private String eventId;
	private String eventName;
	private Date startTime;
	private Date endTime;
	private List<Seat> seats;

	public Event get() {
		return this;
	}

	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
	
	@ApiModelProperty(position = 1, required = true, value = "eventname containing only lowercase letters")
	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public List<Seat> getSeats() {
		return seats;
	}

	public void setSeats(List<Seat> seats) {
		this.seats = seats;
	}

	public void removeSeat(int seatId) {
		if (seats != null) {
			for (Seat seat : seats) {
				if (seat.getSeatId() == seatId) {
					seats.remove(seat);
					break;
				}
			}
		}
	}

	public Seat getSeat(int seatId) {
		if (seats == null)
			return null;
		for (Seat seat : seats) {
			if (seat.getSeatId() == seatId) {
				return seat;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("eventId ");
		sb.append(eventId);
		sb.append(" eventName ");
		sb.append(eventName);
		sb.append(" startTime");
		sb.append(startTime);
		sb.append(" endTime");
		sb.append(endTime);
		if (seats != null) {
			sb.append(" seats ");
			for (Seat seat : seats) {
				sb.append(seat.toString());
				sb.append(" ");
			}
		}
		return sb.toString();
	}

}
