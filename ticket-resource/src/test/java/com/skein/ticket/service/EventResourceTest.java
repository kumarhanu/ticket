package com.skein.ticket.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import junit.framework.Assert;

import org.elasticsearch.common.inject.matcher.Matchers;
import org.jboss.resteasy.core.Dispatcher;
import org.jboss.resteasy.mock.MockDispatcherFactory;
import org.jboss.resteasy.mock.MockHttpRequest;
import org.jboss.resteasy.mock.MockHttpResponse;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.google.gson.Gson;
import com.skein.ticket.bo.EventService;
import com.skein.ticket.domain.Event;

@RunWith(MockitoJUnitRunner.class)
public class EventResourceTest {

	private List<Event> events = null;
	private EventResource eventResource = null;
	private Gson gson = null;

	// No need created instance for this mock. Mockito will create instance at
	// before every method execution.
	@Mock
	private EventService<Event> eventServiceMock = null;

	public EventResourceTest() {
		MockitoAnnotations.initMocks(EventResourceTest.class);
	}

	@Before
	public void setUp() {
		eventResource = new EventResource();

		events = new ArrayList<>();
		Event event = new Event();
		event.setEventId("eventId");
		event.setEventName("eventName");

		events.add(event);

		gson = new Gson();
	}

	@After
	public void tearDown() {
		eventResource = null;
		events = null;
		gson = null;
	}

	@Test
	public void testGetAll() throws URISyntaxException {

		Mockito.when(eventServiceMock.getAllEvents()).thenReturn(events);

		// Set EventService Mock to Even Resource object
		ReflectionTestUtils.setField(eventResource, "eventService",
				eventServiceMock);

		Dispatcher dispatcher = MockDispatcherFactory.createDispatcher();
		dispatcher.getRegistry().addSingletonResource(eventResource);
		MockHttpRequest request = MockHttpRequest.get("/events");
		MockHttpResponse response = new MockHttpResponse();

		dispatcher.invoke(request, response);

		// Verifying whether getAllEvents() gets call exactly once.
		verify(eventServiceMock, times(1)).getAllEvents();
		
		Assert.assertEquals(HttpServletResponse.SC_OK, response.getStatus());
		Assert.assertEquals(gson.toJson(events), response.getContentAsString());
	}

}
