package com.skein.ticket.service;

import java.net.URI;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.skein.ticket.bo.UserService;
import com.skein.ticket.domain.User;

@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/users")
@Component
public class UserResource {

	@Autowired
	private UserService<User> userService;
	
	@GET
	public List<User> get(){
		return userService.getAllUsers();
	}
	
	@POST
	public Response create(User user, @Context UriInfo uriInfo){
		String userId = userService.create(user);
		
		URI newURI = uriInfo.getBaseUriBuilder().path("users/{"+userId+"}").build(userId);
		
		return Response.created(newURI).build();
	}
	
	@Path("/{user_id}")
	public User getUser(@PathParam ("user_id") String userId){
		return userService.findById(userId);
	}
	
	@Path("/{user_id}/event/{event_name}/ticket")
	public EventResource getTicket(@PathParam ("user_id") String userId, @PathParam ("event_name") String eventName){
		return new EventResource();
	}
}
