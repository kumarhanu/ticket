package com.skein.ticket.service;

import java.net.URI;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.skein.ticket.bo.TicketService;
import com.skein.ticket.core.patch.PATCH;
import com.skein.ticket.domain.Event;
import com.skein.ticket.domain.Ticket;

/*
 * SpringContextLoaderListener which is defined in web.xml loads this class at Server startup.
 */

/*
 * http://localhost:8080/ticket-resource/rest/tickets
 */
@Path("/tickets")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Component
public class TicketResource {

	@Autowired
	TicketService<Ticket> ticketService;
	
	@Autowired
	EventResource eventResource;

	@GET
	public Response getAll() {
		List<Ticket> tickets = ticketService.getAllTickets();
		if (tickets == null || tickets.size() == 0) {
			return Response.status(Status.NO_CONTENT).build();
		}
		return Response.ok().entity(tickets).build();
	}

	/*
	 * Create Sub-Resource.
	 * 
	 * This method gets invoked with root URL with POST method. POST is used to
	 * create sub-resource i.e., Ticket. If the same resource (tickets) needs to
	 * be created then use PUT method.
	 * 
	 * Return with Status=201 and new resource location.
	 */
	@POST
	public Response create(Ticket ticket, @Context UriInfo uriInfo) {

		System.out.println(ticket.getReservations().size());
		String ticketId = ticketService.create(ticket);

		if (ticketId == null) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}

		URI newURI = uriInfo.getBaseUriBuilder()
				.path("tickets/{" + ticketId + "}").build(ticketId);

		return Response.created(newURI).build();
	}

	/*
	 * Sub-resource method.
	 * 
	 * Sub-resource method should not annotate with method-name like @GET
	 * 
	 * This method returns Ticket object and framework resolves additional path
	 * on the Ticket object.
	 * 
	 * In case there is NO additional path like
	 * (http://localhost:8080/ticket-resource/rest/tickets/id1) framework
	 * invokes a method with is annotated with @GET on the Ticket object.
	 */
	@GET
	@Path("/{ticket_id}")
	public Response get(@PathParam("ticket_id") String ticketId) {
		Ticket ticket = ticketService.findById(ticketId);

		if (ticket == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.ok().entity(ticket).build();
	}

	/*
	 * Sub-resource locator
	 */

	@Path("/{ticket_id}")
	public EventResource getEvent(@PathParam("ticket_id") String ticketId) {
		//EventResource eventResource = new EventResource();
		eventResource.setTicketId(ticketId);
		return eventResource;
	}

	/*
	 * Update entire object.
	 * 
	 * For this, should invoke GET prior to this call to get the entire object.
	 */
	@PUT
	public Response update(Ticket ticket) {
		ticketService.update(ticket);

		return Response
				.status(Status.OK)
				.entity("Ticket " + ticket.getTicketId()
						+ "updated successfully").build();
	}

	/*
	 * Partial update.
	 * 
	 * For this, should invoke GET prior to this call to get the entire object.
	 * 
	 * FIXME: NOT implemented completely.
	 */
	@PATCH
	@Path("/{id}")
	@Consumes("application/json-patch+json")
	public Response patch(@PathParam("id") String ticketId, Ticket ticket) {

		ticketService.update(ticket);

		return Response
				.status(Status.OK)
				.entity("Ticket " + ticket.getTicketId()
						+ " updated successfully").build();
	}

	@DELETE
	@Path("/{ticket_id}")
	public Response delete(@PathParam("ticket_id") String ticketId) {
		int result = ticketService.delete(ticketId);

		if (result == 1)
			return Response.ok()
					.entity("Ticket " + ticketId + " successfully deleted.")
					.build();
		else
			return Response.status(Status.NOT_FOUND)
					.entity("Ticket " + ticketId + " not deleted.").build();
	}

}
