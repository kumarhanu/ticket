package com.skein.ticket.service;

import java.net.URI;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.skein.ticket.bo.ReservationService;
import com.skein.ticket.domain.Reservation;

@Component
@Path("/reservations")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public class ReservationResource {

	@Autowired
	ReservationService<Reservation> reservationService;

	/*
	 * Example URL: http://localhost:8080/ticket-resource/rest/tickets
	 * 
	 * Response best practice:
	 * (1) In case of success: Status=200(OK) with Response Body
	 * (2) No data found for the given reservationId: Status=204(No Content) without Response Body
	 */
	@GET
	public Response getAll() {
		List<Reservation> reservations = reservationService.getAllReservations();
		if (reservations == null || reservations.size() == 0) {
			return Response.status(Status.NO_CONTENT).build();
		}
		return Response.ok().entity(reservations).build();
	}

	/*
	 * Example URL: http://localhost:8080/ticket-resource/rest/tickets
	 * 
	 * Request best practice:
	 * (1) In case to create Sub Resource, use POST
	 * (2) In case to create this Resource, use PUT
	 * 
	 * Response best practice:
	 * (1) In case of success: Status=201(Created); Set URL of new Resource in Location Header.
	 * (2) In case of asynchronous request initiate and response pending: Return Status=202(Accepted)
	 * (3) In case of failure: Status=500(Internal Service Error)
	 */
	@POST
	public Response create(Reservation reservation, @Context UriInfo uriInfo) {
		String reservationId = reservationService.create(reservation);

		if(reservationId == null){
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
		
		URI newURI = uriInfo.getBaseUriBuilder()
				.path("reservations/{" + reservationId + "}").build(reservationId);

		return Response.created(newURI).build();
	}

	/*
	 * Example URL: http://localhost:8080/ticket-resource/rest/reservations/1
	 * 
	 * Response best practice:
	 * (1) In case of success: Status=200(OK) with Response Body
	 * (2) No data found for the given reservationId: Status=404(Not Found) without Response Body
	 */
	@GET
	@Path("/{reservation_id}")
	public Response get(@PathParam("reservation_id") String reservationId) {
		Reservation reservation = reservationService.findById(reservationId);
		if (reservation == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.ok().entity(reservation).build();
	}


	/*
	 * Example URL: http://localhost:8080/ticket-resource/rest/reservations/1
	 * 
	 * Response best practice:
	 * (1) In case of Success: Status=200(OK) with Response Body
	 * 
	 * (2) In case of asynchronous request initiate and response pending: Return Status=202(Accepted)
	 * 
	 * (3) In case of failure; Return with Status=500(Internal Server Error) without Response Body
	 */

	@PUT
	@Path("/{reservation_id}")
	public Response update(@PathParam("reservation_id") String reservationId, Reservation reservation){
		reservationService.update(reservation);
		return Response.ok().entity("Reservation updated successfully.").build();
	}
	
}