package com.skein.ticket.service;

import java.net.URI;
import java.util.Collection;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.skein.ticket.Constants;
import com.skein.ticket.bo.SeatService;
import com.skein.ticket.core.patch.PATCH;
import com.skein.ticket.domain.Seat;

@Component
@Path("/seats")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public class SeatResource {

	@Autowired
	SeatService<Seat> seatService;

	/*
	 * Example URL: http://localhost:8080/ticket-resource/rest/tickets
	 * 
	 * Response best practice: (1) In case of success: Status=200(OK) with
	 * Response Body (2) No data found for the given seatId: Status=204(No
	 * Content) without Response Body
	 */
	@GET
	public Response getAll(@QueryParam("fields") Collection<String> fields,
			@QueryParam("status") Collection<String> statusFilter,
			@QueryParam("sort") Collection<String> sortFields,
			@QueryParam("offset") int offSet, @QueryParam("limit") int limit) {

		List<Seat> seats = seatService.getAllSeats((List<String>) fields,
				(List<String>) statusFilter, (List<String>) sortFields, offSet,
				limit);

		if (seats == null || seats.size() == 0) {
			return Response.status(Status.NO_CONTENT).build();
		}

		long count = seatService.getCount((List<String>) statusFilter);

		return Response.ok().entity(seats)
				.header(Constants.X_TOTAL_COUNT, count).build();
	}

	/*
	 * Example URL: http://localhost:8080/ticket-resource/rest/tickets
	 * 
	 * Request best practice: (1) In case to create Sub Resource, use POST (2)
	 * In case to create this Resource, use PUT
	 * 
	 * Response best practice: (1) In case of success: Status=201(Created); Set
	 * URL of new Resource in Location Header. (2) In case of asynchronous
	 * request initiate and response pending: Return Status=202(Accepted) (3) In
	 * case of failure: Status=500(Internal Service Error)
	 */
	@POST
	public Response create(Seat seat, @Context UriInfo uriInfo) {
		String seatId = seatService.create(seat);

		if (seatId == null) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}

		URI newURI = uriInfo.getBaseUriBuilder().path("seats/{" + seatId + "}")
				.build(seatId);

		return Response.created(newURI).build();
	}

	/*
	 * Example URL: http://localhost:8080/ticket-resource/rest/seats/1
	 * 
	 * Response best practice: (1) In case of success: Status=200(OK) with
	 * Response Body (2) No data found for the given seatId: Status=404(Not
	 * Found) without Response Body
	 */
	@GET
	@Path("/{seat_id}")
	public Response get(@PathParam("seat_id") String seatId) {
		Seat seat = seatService.findById(seatId);
		if (seat == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.ok().entity(seat).build();
	}

	/*
	 * Example URL: http://localhost:8080/ticket-resource/rest/seats/1
	 * 
	 * Response best practice: (1) In case of Success: Status=200(OK) with
	 * Response Body
	 * 
	 * (2) In case of asynchronous request initiate and response pending: Return
	 * Status=202(Accepted)
	 * 
	 * (3) In case of failure; Return with Status=500(Internal Server Error)
	 * without Response Body
	 */

	@PUT
	@Path("/{seat_id}")
	public Response update(@PathParam("seat_id") String seatId, Seat seat) {
		seatService.update(seat);
		return Response.ok().entity("Seat updated successfully.").build();
	}

	/*
	 * Example URL: http://localhost:8080/ticket-resource/rest/seats/1
	 * 
	 * Response best practice: (1) In case of Success: (a) Status=200(OK) with
	 * Response Body (b) Status=204(No Content) without Response Body
	 * 
	 * (2) In case of asynchronous request initiate and response pending: Return
	 * Status=202(Accepted)
	 * 
	 * (3) In case of failure; Return with Status=404(Not found) without
	 * Response Body
	 */
	@DELETE
	@Path("/{seatId}")
	public Response delete(@PathParam("seatId") String seatId) {
		int result = seatService.delete(seatId);

		if (result == 1)
			return Response.ok()
					.entity("Seat " + seatId + " successfully deleted.")
					.build();
		else
			return Response.status(Status.NOT_FOUND)
					.entity("Seat " + seatId + " not deleted.").build();
	}

	@PATCH
	@Path("/{seat_id}")
	@Consumes("application/json-patch+json")
	public Response patch(@PathParam("seat_id") String seatId, Seat seat) {
		seat.setSeatId(Integer.parseInt(seatId));
		seatService.update(seat);

		return Response.status(Status.OK)
				.entity("Seat " + seatId + " updated successfully").build();
	}

}