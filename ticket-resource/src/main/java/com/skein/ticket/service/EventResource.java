package com.skein.ticket.service;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.map.ObjectMapper;
import org.jboss.resteasy.annotations.GZIP;
import org.jboss.resteasy.annotations.cache.Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.skein.ticket.bo.EventService;
import com.skein.ticket.core.patch.PATCH;
import com.skein.ticket.domain.Event;
import com.skein.ticket.domain.Seat;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

/*
 * SpringContextLoaderListener which is defined in web.xml loads this class at Server startup.
 */
@Path("/events")
@Api(value="/events", description="Endpoint for event management")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Component
public class EventResource {

	@Autowired
	EventService<Event> eventService;
	private String ticketId;

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	/*
	 * Example URL: http://localhost:8080/ticket-resource/rest/tickets
	 * 
	 * Response best practice: (1) In case of success: Status=200(OK) with
	 * Response Body (2) No data found for the given eventId: Status=204(No
	 * Content) without Response Body
	 */
	@GZIP
	@GET
	@ApiOperation(value = "Return all events", response = Response.class, responseContainer = "Object")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful retrieval of events", response = Event.class),
			@ApiResponse(code = 204, message = "No events exists."),
			@ApiResponse(code = 500, message = "Internal server error") })
	public Response getAll() {
		List<Event> events = eventService.getAllEvents();
		if (events == null || events.size() == 0) {
			return Response.status(Status.NO_CONTENT).build();
		}
		return Response.ok().entity(events).build();
	}

	/*
	 * Example URL: http://localhost:8080/ticket-resource/rest/tickets
	 * 
	 * Request best practice: (1) In case to create Sub Resource, use POST (2)
	 * In case to create this Resource, use PUT
	 * 
	 * Response best practice: (1) In case of success: Status=201(Created); Set
	 * URL of new Resource in Location Header. (2) In case of asynchronous
	 * request initiate and response pending: Return Status=202(Accepted) (3) In
	 * case of failure: Status=500(Internal Service Error)
	 */
	@POST
	public Response create(@GZIP Event event, @Context UriInfo uriInfo) {
		String eventId = eventService.create(event);

		if (eventId == null) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}

		URI newURI = uriInfo.getBaseUriBuilder()
				.path("events/{" + eventId + "}").build(eventId);

		return Response.created(newURI).build();
	}

	/*
	 * Example URL: http://localhost:8080/ticket-resource/rest/events/1
	 * 
	 * Response best practice: (1) In case of success: Status=200(OK) with
	 * Response Body (2) No data found for the given eventId: Status=404(Not
	 * Found) without Response Body
	 */
	@Cache(maxAge=100)  //FIXME: Cache not working
	@GET
	@Path("/{event_id}")
	public Response get(@PathParam("event_id") String eventId) {
		Event event = eventService.findById(eventId);
		if (event == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.ok().entity(event).build();
	}

	@Path("/event")
	@GET
	public Response getByTicket() {
		Event event = eventService.findByTicket(this.ticketId);
		if (event == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.ok().entity(event).build();
	}

	/*
	 * Example URL: http://localhost:8080/ticket-resource/rest/events/1
	 * 
	 * Response best practice: (1) In case of Success: Status=200(OK) with
	 * Response Body
	 * 
	 * (2) In case of asynchronous request initiate and response pending: Return
	 * Status=202(Accepted)
	 * 
	 * (3) In case of failure; Return with Status=500(Internal Server Error)
	 * without Response Body
	 */

	@PUT
	@Path("/{event_id}")
	public Response update(@PathParam("event_id") String eventId, Event event) {
		eventService.update(event);
		return Response.ok().entity("Event updated successfully.").build();
	}

	/*
	 * Example URL: http://localhost:8080/ticket-resource/rest/events/1
	 * 
	 * Response best practice: (1) In case of Success: (a) Status=200(OK) with
	 * Response Body (b) Status=204(No Content) without Response Body
	 * 
	 * (2) In case of asynchronous request initiate and response pending: Return
	 * Status=202(Accepted)
	 * 
	 * (3) In case of failure; Return with Status=404(Not found) without
	 * Response Body
	 */
	@DELETE
	@Path("/{event_id}")
	public Response delete(@PathParam("event_id") String eventId) {
		int result = eventService.delete(eventId);

		if (result == 1)
			return Response.ok()
					.entity("Event " + eventId + " successfully deleted.")
					.build();
		else
			return Response.status(Status.NOT_FOUND)
					.entity("Event " + eventId + " not deleted.").build();
	}

	@PATCH
	@Path("/{event_id}")
	@Consumes("application/json-patch+json")
	public Response patch(@PathParam("event_id") String eventId, InputStream is) {
		ObjectMapper mapper = new ObjectMapper();
		Event event = eventService.findById(eventId);
		JsonParser jsonParser = null;
		try {
			jsonParser = mapper.getJsonFactory().createJsonParser(is);

			JsonToken current = jsonParser.nextToken();
			if (current == JsonToken.START_ARRAY) {
				// For each of the records in the array
				while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
					JsonNode node = jsonParser.readValueAsTree();
					System.out.println(node.toString());
					System.out.println(node.get("op"));
					System.out.println(node.get("path"));
					System.out.println(node.get("value"));

					if (node.get("op").getTextValue().contains("replace")) {
						if (node.get("path").getTextValue()
								.contains("eventName")) {
							event.setEventName(node.get("value").getTextValue());
						}
					} else if (node.get("op").getTextValue().contains("add")) {
						if (node.get("path").getTextValue().contains("seat")) {
							JsonNode valueNode = node.get("value");
							Seat seat = mapper.readValue(valueNode, Seat.class);
							System.out.println("seat " + seat.toString());
							System.out.println(event.getSeats().add(seat));
							System.out.println(event);
						}
					} else if (node.get("op").getTextValue().contains("remove")) {
						if (node.get("path").getTextValue().contains("seat")) {
							System.out.println(node.get("value"));
							System.out.println(Integer.parseInt(node.get("value").getTextValue()));
							event.removeSeat(Integer.parseInt(node.get("value").getTextValue()));
							System.out.println(event);
						}
					} else if (node.get("op").getTextValue().contains("test")) {
						if (node.get("path").getTextValue()
								.contains("eventId")) {
							if(node.get("value").getTextValue().contains(event.getEventId())){
								System.out.println("true");
							}
						}
					} else if (node.get("op").getTextValue().contains("copy")) {
						String[] pathSegment = node.get("path").getTextValue().split("/");
						String[] valueSegment = node.get("value").getTextValue().split("/");
						System.out.println(pathSegment[2]);
						Seat sourceSeat = event.getSeat(Integer.parseInt(pathSegment[2]));
						Seat destSeat = event.getSeat(Integer.parseInt(valueSegment[2]));

						if(pathSegment[3].contains("status")){
							destSeat.setStatus(sourceSeat.getStatus());
						}
					} else if (node.get("op").getTextValue().contains("move")) {
						String[] pathSegment = node.get("path").getTextValue().split("/");
						String[] valueSegment = node.get("value").getTextValue().split("/");
						
						Seat sourceSeat = event.getSeat(Integer.parseInt(pathSegment[2]));
						Seat destSeat = event.getSeat(Integer.parseInt(valueSegment[2]));

						if(pathSegment[3].contains("status")){
							destSeat.setStatus(sourceSeat.getStatus());
							sourceSeat.setStatus(null);
						}
					}
				}
			} else {
				System.out
						.println("Error: records should be an array: skipping.");
				jsonParser.skipChildren();
			}
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}

		System.out.println(event);
		eventService.update(event);
		
		return Response.status(Status.OK)
				.entity("Event " + eventId + " updated successfully").build();
	}
}