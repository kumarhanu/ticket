package com.skein.ticket.service;

import java.net.URI;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.skein.ticket.couchbase.UserProfileService;
import com.skein.ticket.couchbase.UserSessionService;

/*
 * SpringContextLoaderListener which is defined in web.xml loads this class at Server startup.
 */

@Path("/userprofiles")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Component
public class UserProfileResource {

	@Autowired
	UserProfileService userProfileService;

	@Autowired
	UserSessionService userSessionService;

	@POST
	public Response create(String userProfileDocument, @Context UriInfo uriInfo) {

		String userProfielId = userProfileService.create(userProfileDocument);

		URI newURI = uriInfo.getBaseUriBuilder()
				.path("userprofiles/{" + userProfielId + "}")
				.build(userProfielId);

		return Response.created(newURI).build();
	}

	@GET
	@Path("/{user_profile_id}")
	public Response get(@PathParam("user_profile_id") String userProfileId) {
		return Response.ok().entity(userProfileService.get(userProfileId))
				.build();
	}

	@GET
	@Path("/{user_profile_id}/sessions")
	public Response getSessions(
			@PathParam("user_profile_id") String userProfileId) {
		return Response.ok().entity(userSessionService.getAll(userProfileId))
				.build();
	}

	@GET
	@Path("/count")
	public Response getCount(@Context UriInfo allUri) {
		MultivaluedMap<String, String> filterMap = allUri.getQueryParameters();
		Map<String, String> map = new HashMap<>();

		for (String key : filterMap.keySet()) {
			map.put(key, filterMap.get(key).get(0));
			System.out.println(key);
			System.out.println(filterMap.get(key).get(0));
		}

		return Response.ok().entity(userProfileService.getCount(map)).build();
	}

	@GET
	public Response getAll(@QueryParam("fields") Collection<String> fields,
			@QueryParam("fbid") Collection<String> fbIdFilter,
			@QueryParam("sort") Collection<String> sortFields,
			@QueryParam("offset") int offSet, @QueryParam("limit") int limit) {

		Collection<Object> userprofiles = userProfileService.getAll((List<String>) fields,
				(List<String>) fbIdFilter, (List<String>) sortFields, offSet,
				limit);

		return Response.ok().entity(userprofiles).build();
	}
}