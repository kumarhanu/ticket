package com.skein.ticket.service;

import java.net.URI;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.skein.ticket.couchbase.UserSessionService;

/*
 * SpringContextLoaderListener which is defined in web.xml loads this class at Server startup.
 */

@Path("/usersessions")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Component
public class UserSessionResource {

	@Autowired
	UserSessionService userSessionService;
	
	@POST
	@Path("/{user_id}")
	public Response create(@PathParam("user_id") String userId, String userSessionDocument, @Context UriInfo uriInfo) {
	
		String userProfielId = userSessionService.create(userId, userSessionDocument);

		URI newURI = uriInfo.getBaseUriBuilder()
				.path("usersessions/{" + userProfielId + "}")
				.build(userProfielId);

		return Response.created(newURI).build();
	}

	@GET
	@Path("/{user_session_id}")
	public Response get(@PathParam("user_session_id") String userSessionId){
		return Response.ok().entity(userSessionService.get(userSessionId)).build();
	}
	
	@GET
	@Path("/{user_session_id}/sessions")
	public Response getAll(@PathParam("user_session_id") String userProfileId){
		return Response.ok().entity(userSessionService.get(userProfileId)).build();
	}
}