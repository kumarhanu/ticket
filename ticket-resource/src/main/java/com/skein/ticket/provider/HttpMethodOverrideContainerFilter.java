package com.skein.ticket.provider;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.ext.Provider;

/*
 * Some clients may not support PUT, PATCH,DELETE. To support these methods accept a Request Header, standard one is ‘X-HTTP-Method-Override’. Should be on POST, not on GET.
 * 
 * PreMatching Filter gets executed before identifying the actual Resource Method to execute.
 */
@PreMatching
@Provider
public class HttpMethodOverrideContainerFilter implements
		ContainerRequestFilter {

	@Override
	public void filter(ContainerRequestContext context) throws IOException {

		String methodName = context.getMethod();
		String overrideMethodName = context.getHeaders().getFirst(
				"X_HTTP_METHOD_OVERRIDE");

		if (overrideMethodName != null && methodName != null
				&& methodName.equalsIgnoreCase("POST")) {
			context.setMethod(overrideMethodName);
		}

	}

}
