package com.skein.ticket.provider;

import java.util.Arrays;
import java.util.Collection;

import javax.ws.rs.ext.Provider;

import org.jboss.resteasy.spi.StringConverter;

/*
 * This converts value of any param in a request (@QueryParam) to a List.
 * 
 * Ex: ?status=1,2 converts into ["1","2"]
 */

@Provider
public class StringCollectionConverter implements StringConverter<Collection<String>> {

	private static final String DELIMETER = ",";
	
    @Override
    public Collection<String> fromString(String string) {
        if (string == null) {
            return null;
        }
        return Arrays.asList(string.split(DELIMETER));
    }

    @Override
    public String toString(Collection<String> values) {
        final StringBuilder sb = new StringBuilder();
        boolean first = true;
        for (String value : values) {
            if (first) {
                first = false;
            } else {
                sb.append(DELIMETER);
            }
            sb.append(value);
        }
        return sb.toString();
    }
}