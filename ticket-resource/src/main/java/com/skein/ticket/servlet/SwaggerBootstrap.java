package com.skein.ticket.servlet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import com.wordnik.swagger.jaxrs.config.BeanConfig;

/*
 * This class is bootstarp for Swagger.
 */
public class SwaggerBootstrap extends HttpServlet {
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);

        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setVersion("1.0.0");
        beanConfig.setBasePath("http://localhost:9966/ticket-resource/rest");
        beanConfig.setResourcePackage("com.skein.ticket.service");
        beanConfig.setScan(true);
    }
}
