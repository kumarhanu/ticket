package com.skein.ticket;


public class Constants {
	public static final String X_TOTAL_COUNT = "X-Total-Count";
	public static final String X_HTTP_METHOD_OVERRIDE = "X-HTTP-Method-Override";
}
