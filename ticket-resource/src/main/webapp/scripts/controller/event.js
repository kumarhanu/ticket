var app = angular.module('ticketApp.controller', []);

app.controller('EventsController', ['$scope', 'EventsService', 'EventService', '$location', function($scope, EventsService, EventService, $location) {
    $scope.message = 'List of Events.';
// $scope.events = [
// {
// eventId : "id1",
// eventName : "name1"
// },
// {
// eventId : "id2",
// eventName : "name2"
// }
// ];
    $scope.editEvent = function (eventId){
    	$location.path('/event-detail/' + eventId);
    };
    
    $scope.createEvent = function(){
    	$location.path('/event-create');
    };
    
    $scope.deleteEvent = function(eventId){
    	EventService.remove({id:eventId});
    	$scope.events = EventsService.query();
    	$scope.$apply();
    };
    
    //Filtering events by given search word in event name.
    $scope.searchFilter = function (event) {
        var keyword = new RegExp($scope.nameFilter, 'i');
        return !$scope.nameFilter || keyword.test(event.eventName);
    };
    
    $scope.events = EventsService.query();
    $scope.$apply();
}]);

app.controller('EventController', ['$scope', '$routeParams', 'EventService', '$location', function($scope, $routeParams, EventService, $location){
	$scope.cancel = function(){
		$location.path('/events');
	};
	
	$scope.updateEvent = function (){
		EventService.update($scope.event);
		$location.path('/events');
	};
	
	$scope.event = EventService.show({id: $routeParams.id});
}]);

app.controller('EventCreateController', ['$scope', 'EventsService','$location', function($scope, EventsService, $location){
	
	$scope.createEvent = function (){
		EventsService.create($scope.event);
		$location.path('/events');
	};
}]);