var ticketApp = angular.module('ticketApp', ['ngRoute', 'ticketApp.controller', 'ticketApp.service']);

ticketApp.config(function ($routeProvider, $httpProvider) {
    $routeProvider
    // route for the events page
    .when('/events', {
        templateUrl : 'partials/events.html',
        controller  : 'EventsController'
    })
    
    .when('/event-detail/:id', {
    	templateUrl : 'partials/event-detail.html',
    	controller  : 'EventController'
    })
    
    .when('/event-create', {
    	templateUrl : 'partials/event-create.html',
    	controller  : 'EventCreateController'
    });
    
    $httpProvider.defaults.useXDomain = true;
});