var app = angular.module('ticketApp.service', ['ngResource']);

var baseUrl = 'http://localhost\\:9966';

app.factory('EventsService', function ($resource){
    return $resource(baseUrl + '/ticket-resource/rest/events', {}, {
        query: { method: 'GET', isArray: true },
    	create: { method: 'POST'}
    });
});

app.factory('EventService', function ($resource){
    return $resource(baseUrl + '/ticket-resource/rest/events/:id', {}, {
    	show: { method: 'GET' },
    	update: { method: 'PUT', params: {id: '@eventId'} },
    	remove: { method: 'DELETE', params: {id: '@eventId'} }
    });
});